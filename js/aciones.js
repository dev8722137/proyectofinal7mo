import {
  ref,
  get,
  set,
  getDatabase,
  update,
} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";
import { app } from "./app.js";

// BOTONES
const btnInsertar = document.getElementById("insertar");
const btnBuscar = document.getElementById("buscar");
const btnActualizar = document.getElementById("actualizar");
const btnLimpiar = document.getElementById("limpiar");
const btnEliminar = document.getElementById("eliminar");
const imagePreview = document.getElementById("imagePreview");
let hospedaje = "";
let nombreCat = "";
let idCat = "";

// INSERTAR PRODUCTO
btnInsertar.addEventListener("click", () => {
  isExiste();
});

// BUSCAR
btnBuscar.addEventListener("click", () => {
  buscar();
});

// ACTUALIZAR
btnActualizar.addEventListener("click", () => {
  alert("No esta permitido actualizar Imagenes hasta proximas actualizaciones del sistema.");
  actualizarDatos();
});

// ELIMINAR
btnEliminar.addEventListener('click', () => {
  deshabilitar();
});

// LIMPIAR
btnLimpiar.addEventListener("click", () => {
  console.log("limpiar");
  limpiarInputs();
});

function limpiarInputs() {
  document.getElementById("id").value = 0;
  document.getElementById("nombre").value = "";
  document.getElementById("categoria").value = 0;
  document.getElementById("desc").value = "";
  document.getElementById("dias").value = 0;
  document.getElementById("fecha").value = "";
  document.getElementById("hospedaje").value = "No";
  document.getElementById("personas").value = 0;
  document.getElementById("precio").value = 0;
  document.getElementById("status").value = "Activo";
  document.getElementById("url").value = "";
  document.getElementById("imageInput").value = "";
}

function llenarInputs(producto) {
  document.getElementById("id").value = producto.id;
  document.getElementById("nombre").value = producto.nombre;
  document.getElementById("precio").value = producto.precio;
  document.getElementById("status").value = producto.status;
  let aux = producto.categoria.id;
  switch (aux) {
    case "asia":
      document.getElementById("categoria").value = 1;
      document.getElementById("categoria").value = 1;
      break;
    case "america":
      document.getElementById("categoria").value = 2;
      document.getElementById("categoria").value = 2;
      break;
    case "africa":
      document.getElementById("categoria").value = 3;
      document.getElementById("categoria").value = 3;
      break;
    case "oceania":
      document.getElementById("categoria").value = 4;
      document.getElementById("categoria").value = 4;
      break;
    case "europa":
      document.getElementById("categoria").value = 5;
      document.getElementById("categoria").value = 5;
      break;
  }

  document.getElementById("desc").value = producto.desc;
  document.getElementById("dias").value = producto.dias;

  let aux2 = producto.hospedaje;
  console.log(aux2);
  if (aux2 == "1") {
    document.getElementById("hospedaje").value = 1;
  } else {
    document.getElementById("hospedaje").value = 2;
  }

  document.getElementById("personas").value = producto.personas;
  document.getElementById("fecha").value = producto.fecha;
  imagePreview.src = producto.url;
}

// Obetener valores
function leerInputs() {
  let id = document.getElementById("id").value;
  let nombre = document.getElementById("nombre").value;
  let precio = document.getElementById("precio").value;
  let status = document.getElementById("status").value;
  let url = document.getElementById("url").value;
  let aux = document.getElementById("categoria").value;
  switch (aux) {
    case "1":
      nombreCat = "Asia";
      idCat = "asia";
      break;
    case "2":
      nombreCat = "América";
      idCat = "america";
      break;
    case "3":
      nombreCat = "Africa";
      idCat = "africa";
      break;
    case "4":
      nombreCat = "Oceania";
      idCat = "oceania";
      break;
    case "5":
      nombreCat = "Europa";
      idCat = "europa";
      break;
  }

  let desc = document.getElementById("desc").value;
  let dias = document.getElementById("dias").value;
  let aux2 = document.getElementById("hospedaje").value;
  switch (aux2) {
    case "1":
      hospedaje = 1;
      break;
    case "2":
      hospedaje = 2;
      break;
  }
  let personas = document.getElementById("personas").value;
  let fecha = document.getElementById("fecha").value;
  return {
    id,
    nombre,
    precio,
    status,
    url,
    nombreCat,
    idCat,
    desc,
    dias,
    hospedaje,
    personas,
    fecha,
  };
}

// Validar si existe el producto
function isExiste() {
  const db = getDatabase(app);
  var read = leerInputs();
  var exist = false;
  if (isEmpty()) {
    alert("No deje espacios vacios");
  } else {
    const dbref = ref(db, "viajes");
    get(dbref)
      .then((snapshot) => {
        if (snapshot.exists()) {
          const productosData = snapshot.val();
          if (productosData !== null) {
            // Verificar si algún producto tiene el código que estás buscando

            // Iterar sobre los productos
            Object.values(productosData).forEach((producto) => {
              if (producto.id === read.id) {
                exist = true;
              }
            });
            if (exist === true) {
              buscar(productosData);
              exist = false;
              alert("Ese Id ya existe");
            } else {
              insertarDatos();
            }
          }
        } else {
          insertarDatos();
        }
      })
      .catch((error) => {
        alert("Error " + error);
      });
  }
}

function isEmpty() {
  var read = leerInputs();
  if (
    read.id == "" ||
    read.nombre == "" ||
    read.precio == "" ||
    read.status == "" ||
    read.nombre == "" ||
    read.desc == "" ||
    read.status == "" 
  ) {
    return true;
  } else {
    return false;
  }
}

function valiEmptyCode() {
  var read = leerInputs();
  if (read.id == "") {
    return true;
  } else {
    return false;
  }
}

async function insertarDatos() {
  var inputs = leerInputs();
  var id = await idInsertar();
  const db = getDatabase();
  set(ref(db, "viajes/" + id), {
    id: inputs.id,
    nombre: inputs.nombre,
    precio: inputs.precio,
    status: inputs.status,
    url: inputs.url,
    categoria: {
      nombre: inputs.nombreCat,
      id: inputs.idCat,
    },
    desc: inputs.desc,
    dias: inputs.dias,
    hospedaje: inputs.hospedaje,
    personas: inputs.personas,
    fecha: inputs.fecha,
  })
    .then((resp) => {
      alert("Se realizo el Registro");
      limpiarInputs();
    })
    .catch((error) => {
      alert("Error" + " " + error);
    });
}

async function buscar(act) {
  if (valiEmptyCode()) {
    alert("Porfavor Ingrese un id para buscar");
  } else {
    const db = getDatabase(app);
    const dbref = ref(db, "viajes");
    var read = leerInputs();
    var exist = false;
    var idcount = 0;

    return new Promise((resolve, reject) => {
      get(dbref)
        .then((snapshot) => {
          if (snapshot.exists()) {
            const productosData = snapshot.val();
            if (productosData !== null) {
              Object.values(productosData).forEach((producto) => {
                if (producto.id === read.id) {
                  exist = true;
                  if (act == true) {
                    act = false;
                    resolve(idcount);
                  }
                }

                idcount++;
              });
              if (exist === true) {
                Object.values(productosData).forEach((producto) => {
                  if (producto.id === read.id) {
                    llenarInputs(producto);
                  }
                });
                exist = false;
              } else {
                alert("Ese id no existe");
              }
            }
          }
        })
        .catch((error) => {
          alert("Error A: " + error);
        });
    });
  }
}

function idInsertar() {
  const db = getDatabase(app);
  const productosRef = ref(db, "viajes");
  let cantidad = 0;
  return obtenerCantidadProductos(cantidad);
  async function obtenerCantidadProductos(cantidad) {
    const snapshot = await get(productosRef);
    if (snapshot.exists()) {
      const productosData = snapshot.val();
      if (productosData !== null) {
        cantidad = Object.keys(productosData).length;
      }
    }
    return cantidad;
  }
}

// ACTUALIZACIÓN DE DATOS

async function actualizarDatos() {
  const db = getDatabase(app);
  var read = leerInputs();
  if (isEmpty()) {
    alert("No deje espacios vacios");
  } else {
    var act = true;
    var id = await buscar(act);
    update(ref(db, "viajes/" + id), {
      id: read.id,
      nombre: read.nombre,
      precio: read.precio,
      status: read.status,
      // url: read.url,
      categoria: {
        nombre: read.nombreCat,
        id: read.idCat,
      },
      desc: read.desc,
      dias: read.dias,
      hospedaje: read.hospedaje,
      personas: read.personas,
      fecha: read.fecha,
    })
      .then((resp) => {
        alert("Se realizo la actualizacion con exito");
        limpiarInputs();
      })
      .catch((error) => {
        alert("Error " + error);
      });
  }
}

// DESHABILITAR

async function deshabilitar() {
    const db = getDatabase(app);
    var read = leerInputs();
    read.status = "true";
    if (isEmpty()) {
        alert('No deje espacios vacios');
    }
    else {
        var act = true;
        var id = await buscar(act);
        update(ref(db, 'viajes/'+id), {
            status: "false",
        }).then(resp => {
            alert('Se realizo la actualizacion con exito');
            limpiarInputs();
        }).catch((error) => {
            alert('Error ' + error);
        })
    }
}
