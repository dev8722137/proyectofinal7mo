import {
  getAuth,
  signInWithEmailAndPassword,
  createUserWithEmailAndPassword,
} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";

const container = document.getElementById("container");
const registerBtn = document.getElementById("register");
const loginBtn = document.getElementById("login");

registerBtn.addEventListener("click", () => {
  container.classList.add("active");
});

loginBtn.addEventListener("click", () => {
  container.classList.remove("active");
});

// Autenticación con Firebase
const btnLogin = document.querySelector("#btnIniciar");
const errorMessage = document.getElementById("error-message");

btnLogin.addEventListener("click", async (e) => {
  e.preventDefault();

  const email = document.getElementById("email-Iniciar").value;
  const password = document.getElementById("password-Iniciar").value;
  const auth = getAuth();

  try {
    await signInWithEmailAndPassword(auth, email, password);
    window.location.href = "../html/admin.html";
  } catch (error) {
    let errorMessage = "llene todos los campos o Contraseña incorrecta.";
    if (error.code === "auth/wrong-password") {
      errorMessage = "La contraseña es incorrecta. Intente Nuevamente";
    } else if (error.errorMessage) {
      errorMessage = error.errorMessage;
    }
    alert(errorMessage);
  }
});

// Registro con Firebase

const btnRegistrar = document.querySelector("#btnRegistrar");
btnRegistrar.addEventListener("click", (e) => {
  e.preventDefault();

  const email = document.getElementById("email-Registro").value;
  const password = document.getElementById("password-Resgistro").value;

  const auth = getAuth();

  createUserWithEmailAndPassword(auth, email, password).then(
    (userCredential) => {
      window.location.href = "../html/admin.html";
    }
  ).catch((error) => {
    const errorCode = error.code;
    const errorMessage = error.message;
  
    alert(errorMessage);
  });

});
