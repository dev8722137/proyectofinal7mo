import { getAuth } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";


// // Cerrar sesión
const auth = getAuth();
const salir = document.querySelector("#salir");

salir.addEventListener("click", async (e) => {
  e.preventDefault();

  auth.signOut().then(() => {
    window.location.href = "../html/tienda.html";
  });
});

