// Import the functions you need from the SDKs you need
import { initializeApp } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-app.js";
import { getAuth} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-auth.js";
import { getStorage} from "https://www.gstatic.com/firebasejs/10.5.2/firebase-storage.js";
import { getDatabase  } from "https://www.gstatic.com/firebasejs/10.5.2/firebase-database.js";


// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyApbl-AfBWtdDtOJQMLVjQoKR2uTcwNKqA",
  authDomain: "proyectofinal-d26b4.firebaseapp.com",
  projectId: "proyectofinal-d26b4",
  storageBucket: "proyectofinal-d26b4.appspot.com",
  messagingSenderId: "316241026740",
  appId: "1:316241026740:web:81044aa9b83bacee1301e1"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
export {app}
export const auth = getAuth(app);
export const storage = getStorage(app);
export const db = getDatabase(app);